/* eslint-disable react/prop-types */
/* eslint-disable no-unused-vars */
import { useState, useEffect } from 'react';

const AnnouncementTable = () => {
  const [announcements, setAnnouncements] = useState([]);

  useEffect(() => {
    fetch('http://localhost:3000/data.json')
      .then((res) => res.json())
      .then((data) => {
        let items = data.map((item) => {
          return (
            <tr key={item.id}>
              <td className="long-text text-start">{item.title}</td>
              <td className="long-text message text-start">
                <span>{item.message}</span>
              </td>
              <td className="text-start">{item.sentBy}</td>
              <td className="text-start">{item.sentThrough}</td>
              <td className="text-start">{item.dateCreated}</td>
              <td className="text-start">{item.startDate}</td>
              <td className="text-start">{item.endDate}</td>
            </tr>
          );
        });
        setAnnouncements(items);
      });
  }, []);

  return (
    <div className="table-responsive-md border">
      <table className="table">
        <thead>
          <tr>
            <th scope="col" className="text-start text-uppercase">
              Title
            </th>
            <th scope="col" className="text-start text-uppercase">
              Message
            </th>
            <th scope="col" className="text-start text-uppercase">
              Sent By
            </th>
            <th scope="col" className="text-start text-uppercase">
              Sent Through
            </th>
            <th scope="col" className="text-start text-uppercase">
              Date Created
            </th>
            <th scope="col" className="text-start text-uppercase">
              Start Date
            </th>
            <th scope="col" className="text-start text-uppercase">
              End Date
            </th>
          </tr>
        </thead>

        <tbody>{announcements}</tbody>
      </table>
    </div>
  );
};

export default AnnouncementTable;
