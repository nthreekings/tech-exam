import React from 'react';

const FilterDropdown = () => {
  return (
    <select className="form-select form-select-sm" aria-label="Filter by">
      <option selected>Filter by</option>
      <option value="1">Oldest</option>
      <option value="2">Newest</option>
    </select>
  );
};

export default FilterDropdown;
