import React from 'react';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const SearchInput = () => {
  return (
    <div className="input-group mb-3">
      <input
        type="text"
        className="form-control form-control-sm search-input"
        aria-label="Search"
      />
      <span className="input-group-text">
        <FontAwesomeIcon icon={faSearch} />
      </span>
    </div>
  );
};

export default SearchInput;
