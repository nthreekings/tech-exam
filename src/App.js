import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Container } from 'react-bootstrap';
import Header from './components/header';
import Announcements from './modules/announcements';

function App() {
  return (
    <div className="App">
      <Header />
      <Container>
        <Announcements />
      </Container>
    </div>
  );
}

export default App;
