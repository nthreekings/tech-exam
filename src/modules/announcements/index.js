import React from 'react';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import SearchInput from '../../components/inputs/search-input';
import FilterDropdown from '../../components/inputs/filter-dropdown';
import AnnouncementTable from '../../components/tables/announcement';

const Tabs = () => {
  return (
    <ul className="nav nav-pills">
      <li className="nav-item pill-1">
        <a className="nav-link active" aria-current="page" href="#">
          <span className="badge rounded-pill bg-success me-2">10</span>All
        </a>
      </li>
      <li className="nav-item pill-2">
        <a className="nav-link" href="#">
          <span className="badge rounded-pill bg-new me-2">10</span>Drafts
        </a>
      </li>
    </ul>
  );
};

const Toolbar = () => {
  return (
    <div className="row">
      <div className="col-md-6">
        <Tabs />
      </div>

      <div className="col-md-3">
        <FilterDropdown />
      </div>

      <div className="col-md-3">
        <SearchInput />
      </div>
    </div>
  );
};

const Announcements = () => {
  return (
    <div className="my-5">
      <div className="row mb-3">
        <div className="col-sm-12 col-md">
          <p className="fs-4 fw-bold text-start mb-0">Announcements</p>
          <p className="text-start">
            View, create, or edit announcements for all employees of your
            company.
          </p>
        </div>
        <div className="col-sm-12 col-md d-grid align-content-center justify-content-md-end">
          <button type="button" className="btn btn-sm btn-success me-md-2">
            <FontAwesomeIcon icon={faPlus} /> Post an Announcement
          </button>
        </div>
      </div>

      <div>
        <Toolbar />
      </div>

      <AnnouncementTable />
    </div>
  );
};

export default Announcements;
